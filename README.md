# Scaleway + Wordpress

Création d'un serveur sur Scaleway et installation de Wordpress.

Playbook Ansible basé sur [tucsonlabs/ansible-playbook-wordpress-nginx](https://github.com/tucsonlabs/ansible-playbook-wordpress-nginx) et modifié pour que ça marche sur Ubuntu Bionic.

## Création serveur

### Étape 1

![screenshot0.png](screenshot/screenshot0.png)

On commence par créer un serveur.

### Étape 2

![screenshot1.png](screenshot/screenshot1.png)

On choisi une Ubuntu Bionic comme image, car c'est sur cette distribution que playbook Ansible est fait.

On choisi comme on veut la région.

### Étape 3

![screenshot2.png](screenshot/screenshot2.png)

Puis on choisi sont type d'instance, ici on choisi la moins chère pour faire des tests.

### Étape 4

![screenshot3.png](screenshot/screenshot3.png)

Si on veut, on peut rajouter de l'espace disque ou différents tags.

### Étape 5

![screenshot4.png](screenshot/screenshot4.png)

Puis on crée le serveur et on attend qu'il soit instancié.

### Étape 6

![screenshot5.png](screenshot/screenshot5.png)

Quand c'est bon, on récupère l'IP publique et on la met dans le fichier `hosts`.

### Étape 7

On peut maintenant installer Wordpress sur ce serveur via Ansible :

~~~
ansible-playbook -u root -i hosts playbook.yml
~~~

### Étape 8

![screenshot6.png](screenshot/screenshot6.png)

Quand Ansible a terminé, on se connecte sur l'IP sur serveur pour finir l'installation de Wordpress.

### Étape 9

![screenshot7.png](screenshot/screenshot7.png)

Puis on rentre les informations pour créer l'administrateur du site.

### Étape 10

![screenshot8.png](screenshot/screenshot8.jpg)

Au final, on a un beau site avec Wordpress !
